# Zodiaque
Webextension pour Firefox (compatible Chrome)

Affiche thème **zodiacal** et tableau annuel des **transits**, natal et progressé, sauve en local les coordonnées de naissance avec option import/export au format json.

_Note_: je suis intéressé si quelqu'un sait comment calculer les positions des planètes (+ noeud lunaire vrai et Lilith) avec une précision < 1' (voir js/calculs.js).


Webextension for Firefox (compatible with Chrome thanks to https://github.com/mozilla/webextension-polyfill)

Displays **zodiacal** theme and annual **transits** both natal and progressed,saves locally birth coordinates with json import/export option.

_Note_: I am interested if someone knows how to calculate the positions of the planets (+ true lunar node and Lilith) with a precision < 1' (see js/calculs.js).
